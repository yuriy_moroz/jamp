package com.epam.mentorship.persistence.dao.impl;

import com.epam.mentorship.persistence.dao.OfficeDao;
import com.epam.mentorship.persistence.Office;

import java.util.ArrayList;
import java.util.List;

public class OfficeDaoImpl implements OfficeDao {

    private DomainStorage storage;

    public OfficeDaoImpl() {
        storage = DomainStorage.getInstance();
    }

    @Override
    public Office create(Office entity) {
        Integer id = storage.getOfficeCounter().incrementAndGet();
        entity.setId(id);
        return storage.getOfficeMap().put(id, entity);
    }

    @Override
    public Office update(Office entity) {
        return storage.getOfficeMap().put(entity.getId(), entity);
    }

    @Override
    public Office findById(Integer id) {
        return storage.getOfficeMap().get(id);
    }

    @Override
    public List<Office> findAll() {
        return new ArrayList<>(storage.getOfficeMap().values());
    }

    @Override
    public Office delete(Integer id) {
        return storage.getOfficeMap().remove(id);
    }
}
