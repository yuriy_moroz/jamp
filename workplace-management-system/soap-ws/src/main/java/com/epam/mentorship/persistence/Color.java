package com.epam.mentorship.persistence;

public enum Color {

    RED,
    BLUE,
    WHITE,
    BLACK,
    GREEN;

    public String value() {
        return name();
    }

    public static Color fromValue(String v) {
        return valueOf(v);
    }

}
