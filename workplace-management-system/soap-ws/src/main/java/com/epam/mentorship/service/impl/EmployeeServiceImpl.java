package com.epam.mentorship.service.impl;

import com.epam.mentorship.persistence.dao.DomainFactory;
import com.epam.mentorship.persistence.dao.impl.DomainFactoryImpl;
import com.epam.mentorship.persistence.Employee;
import com.epam.mentorship.persistence.PrimarySkill;
import com.epam.mentorship.presentation.domain.EmployeeType;
import com.epam.mentorship.presentation.domain.PrimarySkillType;
import com.epam.mentorship.presentation.exception.EmployeeNotFoundException;
import com.epam.mentorship.service.EmployeeService;

import java.util.ArrayList;
import java.util.List;

public class EmployeeServiceImpl implements EmployeeService {

    private DomainFactory factory;

    public EmployeeServiceImpl() {
        factory = new DomainFactoryImpl();
    }

    @Override
    public void convertToPersistenceModel(EmployeeType from, Employee to) {
        to.setName(from.getName());
        to.setPosition(from.getPosition());
        to.setPrimarySkill(PrimarySkill.fromValue(from.getPrimarySkill().value()));
    }

    @Override
    public void convertToPresentationModel(Employee from, EmployeeType to) {
        to.setId(from.getId());
        to.setName(from.getName());
        to.setPrimarySkill(PrimarySkillType.fromValue(from.getPrimarySkill().value()));
        to.setPosition(from.getPosition());
    }

    @Override
    public EmployeeType add(EmployeeType entity) {
        Employee employee = new Employee();
        convertToPersistenceModel(entity, employee);
        factory.getEmployeeDao().create(employee);
        entity.setId(employee.getId());
        return entity;
    }

    @Override
    public EmployeeType update(EmployeeType entity) throws EmployeeNotFoundException {
        Employee employee = factory.getEmployeeDao().findById(entity.getId());
        if (null == employee) {
            throw new EmployeeNotFoundException(String.valueOf(employee.getId()));
        }
        convertToPersistenceModel(entity, employee);
        factory.getEmployeeDao().update(employee);
        return entity;
    }

    @Override
    public EmployeeType get(Integer id) throws EmployeeNotFoundException {
        Employee employee = factory.getEmployeeDao().findById(id);
        if (null == employee) {
            throw new EmployeeNotFoundException(id.toString());
        }
        EmployeeType employeeType = new EmployeeType();
        convertToPresentationModel(employee, employeeType);
        return employeeType;
    }

    @Override
    public List<EmployeeType> getAll() {
        List<Employee> employees = factory.getEmployeeDao().findAll();
        List<EmployeeType> employeeTypes = new ArrayList<>(employees.size());
        if (!employees.isEmpty()) {
            for (Employee employee : employees) {
                EmployeeType employeeType = new EmployeeType();
                convertToPresentationModel(employee, employeeType);
                employeeTypes.add(employeeType);
            }
        }
        return employeeTypes;
    }

    @Override
    public EmployeeType delete(Integer id) {
        Employee employee = factory.getEmployeeDao().delete(id);
        EmployeeType employeeType = new EmployeeType();
        if (null == employee) {
            return employeeType;
        }
        convertToPresentationModel(employee, employeeType);
        return employeeType;
    }
}
