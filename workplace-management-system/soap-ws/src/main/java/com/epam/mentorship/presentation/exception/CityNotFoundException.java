package com.epam.mentorship.presentation.exception;

public class CityNotFoundException extends Exception {
    public CityNotFoundException(String id) {
        super("City isn't found for id "+id);
    }
}
