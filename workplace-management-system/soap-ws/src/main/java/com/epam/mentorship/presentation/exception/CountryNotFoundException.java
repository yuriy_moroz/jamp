package com.epam.mentorship.presentation.exception;

public class CountryNotFoundException extends Exception {
    public CountryNotFoundException(String name) {
        super("Country isn't found for name "+name);
    }
}
