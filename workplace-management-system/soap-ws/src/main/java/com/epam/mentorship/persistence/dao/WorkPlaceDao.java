package com.epam.mentorship.persistence.dao;

import com.epam.mentorship.persistence.WorkPlaceDomain;

public interface WorkPlaceDao extends CrudDao<Integer, WorkPlaceDomain> {
}
