package com.epam.mentorship.persistence.dao.impl;

import com.epam.mentorship.persistence.dao.EmployeeDao;
import com.epam.mentorship.persistence.Employee;

import java.util.ArrayList;
import java.util.List;

public class EmployeeDaoImpl implements EmployeeDao {

    private DomainStorage storage;

    public EmployeeDaoImpl() {
        storage = DomainStorage.getInstance();
    }

    @Override
    public Employee create(Employee entity) {
        Integer id = storage.getEmployeeCounter().incrementAndGet();
        entity.setId(id);
        return storage.getEmployeeMap().put(id, entity);
    }

    @Override
    public Employee update(Employee entity) {
        return storage.getEmployeeMap().put(entity.getId(), entity);
    }

    @Override
    public Employee findById(Integer id) {
        return storage.getEmployeeMap().get(id);
    }

    @Override
    public List<Employee> findAll() {
        return new ArrayList<>(storage.getEmployeeMap().values());
    }

    @Override
    public Employee delete(Integer id) {
        return storage.getEmployeeMap().remove(id);
    }
}
