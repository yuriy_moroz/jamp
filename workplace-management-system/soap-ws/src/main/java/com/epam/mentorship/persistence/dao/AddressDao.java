package com.epam.mentorship.persistence.dao;

import com.epam.mentorship.persistence.Address;

public interface AddressDao extends CrudDao<Integer, Address> {
}
