package com.epam.mentorship.persistence;

public class Area {

    private Integer id;
    private String name;
    private Office office;
    private int square;
    private Integer floor;
    private int employeeCount;
    private int roomCount;
    private Integer meetingRoomCount;
    private Color color;

    public Area(Integer id, String name, Office office, int square,
                Integer floor, int employeeCount, int roomCount,
                Integer meetingRoomCount, Color color) {
        this.id = id;
        this.name = name;
        this.office = office;
        this.square = square;
        this.floor = floor;
        this.employeeCount = employeeCount;
        this.roomCount = roomCount;
        this.meetingRoomCount = meetingRoomCount;
        this.color = color;
    }

    public Area() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Office getOffice() {
        return office;
    }

    public void setOffice(Office office) {
        this.office = office;
    }

    public int getSquare() {
        return square;
    }

    public void setSquare(int square) {
        this.square = square;
    }

    public Integer getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public int getEmployeeCount() {
        return employeeCount;
    }

    public void setEmployeeCount(int employeeCount) {
        this.employeeCount = employeeCount;
    }

    public int getRoomCount() {
        return roomCount;
    }

    public void setRoomCount(int roomCount) {
        this.roomCount = roomCount;
    }

    public Integer getMeetingRoomCount() {
        return meetingRoomCount;
    }

    public void setMeetingRoomCount(Integer meetingRoomCount) {
        this.meetingRoomCount = meetingRoomCount;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
