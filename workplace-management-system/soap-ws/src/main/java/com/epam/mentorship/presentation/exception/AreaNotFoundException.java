package com.epam.mentorship.presentation.exception;

public class AreaNotFoundException extends Exception {
    public AreaNotFoundException(String id) {
        super("Area isn't found for id "+id);
    }
}
