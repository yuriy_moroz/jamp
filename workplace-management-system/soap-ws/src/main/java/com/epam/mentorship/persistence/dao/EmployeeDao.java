package com.epam.mentorship.persistence.dao;

import com.epam.mentorship.persistence.Employee;

public interface EmployeeDao extends CrudDao<Integer, Employee> {
}
