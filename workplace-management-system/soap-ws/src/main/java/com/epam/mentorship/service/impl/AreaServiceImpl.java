package com.epam.mentorship.service.impl;

import com.epam.mentorship.persistence.dao.DomainFactory;
import com.epam.mentorship.persistence.dao.impl.DomainFactoryImpl;
import com.epam.mentorship.persistence.Area;
import com.epam.mentorship.persistence.Color;
import com.epam.mentorship.presentation.domain.AreaType;
import com.epam.mentorship.presentation.domain.ColorSimpleType;
import com.epam.mentorship.presentation.exception.AreaNotFoundException;
import com.epam.mentorship.service.AreaService;

import java.util.ArrayList;
import java.util.List;

public class AreaServiceImpl implements AreaService {

    private DomainFactory factory;

    public AreaServiceImpl() {
        factory = new DomainFactoryImpl();
    }

    @Override
    public void convertToPersistenceModel(AreaType from, Area to) {
        to.setName(from.getName());
        to.setEmployeeCount(from.getEmployeeCount());
        to.setSquare(from.getSquare());
        to.setColor(Color.fromValue(from.getColor().value()));
        to.setFloor(from.getFloor());
        to.setMeetingRoomCount(from.getMeetingRoomCount());
        to.setRoomCount(from.getRoomCount());
        to.setOffice(factory.getOfficeDao().findById(from.getOfficeId()));
    }

    @Override
    public void convertToPresentationModel(Area from, AreaType to) {
        to.setId(from.getId());
        to.setName(from.getName());
        to.setEmployeeCount(from.getEmployeeCount());
        to.setFloor(from.getFloor());
        to.setSquare(from.getSquare());
        to.setRoomCount(from.getRoomCount());
        to.setMeetingRoomCount(from.getMeetingRoomCount());
        to.setOfficeId(from.getOffice().getId());
        to.setColor(ColorSimpleType.fromValue(from.getColor().value()));
    }

    @Override
    public AreaType add(AreaType entity) {
        Area area = new Area();
        convertToPersistenceModel(entity, area);
        factory.getAreaDao().create(area);
        entity.setId(area.getId());
        return entity;
    }

    @Override
    public AreaType update(AreaType entity) throws AreaNotFoundException {
        Area area = factory.getAreaDao().findById(entity.getId());
        if (null == area) {
            throw new AreaNotFoundException(String.valueOf(entity.getId()));
        }
        convertToPersistenceModel(entity, area);
        factory.getAreaDao().update(area);
        return entity;
    }

    @Override
    public AreaType get(Integer id) throws AreaNotFoundException {
        Area area = factory.getAreaDao().findById(id);
        if (null == area) {
            throw new AreaNotFoundException(id.toString());
        }
        AreaType areaType = new AreaType();
        convertToPresentationModel(area, areaType);
        return areaType;
    }

    @Override
    public List<AreaType> getAll() {
        List<Area> areas = factory.getAreaDao().findAll();
        List<AreaType> areaTypes = new ArrayList<>(areas.size());
        if (!areas.isEmpty()) {
            for (Area area : areas) {
                AreaType areaType = new AreaType();
                convertToPresentationModel(area, areaType);
                areaTypes.add(areaType);
            }
        }
        return areaTypes;
    }

    @Override
    public AreaType delete(Integer id) {
        Area area = factory.getAreaDao().delete(id);
        AreaType areaType = new AreaType();
        if (null != area) {
            convertToPresentationModel(area, areaType);
        }
        return areaType;
    }
}
