package com.epam.mentorship.service.impl;

import com.epam.mentorship.persistence.dao.DomainFactory;
import com.epam.mentorship.persistence.dao.impl.DomainFactoryImpl;
import com.epam.mentorship.persistence.City;
import com.epam.mentorship.presentation.domain.CityType;
import com.epam.mentorship.presentation.exception.CityNotFoundException;
import com.epam.mentorship.service.CityService;

import java.util.ArrayList;
import java.util.List;

public class CityServiceImpl implements CityService {

    private DomainFactory factory;

    public CityServiceImpl() {
        factory = new DomainFactoryImpl();
    }

    @Override
    public void convertToPersistenceModel(CityType from, City to) {
        to.setCountry(factory.getCountryDao().findById(from.getCountryName()));
        to.setName(from.getName());
    }

    @Override
    public void convertToPresentationModel(City from, CityType to) {
        to.setCountryName(from.getCountry().getName());
        to.setId(from.getId());
        to.setName(from.getName());
    }

    @Override
    public CityType add(CityType entity) {
        City city = new City();
        convertToPersistenceModel(entity, city);
        factory.getCityDao().create(city);
        entity.setId(city.getId());
        return entity;
    }

    @Override
    public CityType update(CityType entity) throws CityNotFoundException {
        City city = factory.getCityDao().findById(entity.getId());
        if (null == city) {
            throw new CityNotFoundException(String.valueOf(entity.getId()));
        }
        convertToPersistenceModel(entity, city);
        factory.getCityDao().update(city);
        return entity;
    }

    @Override
    public CityType get(Integer id) throws CityNotFoundException {
        CityType cityType = new CityType();
        City city = factory.getCityDao().findById(id);
        if (null == city) {
            throw new CityNotFoundException(id.toString());
        }
        convertToPresentationModel(city, cityType);
        return cityType;
    }

    @Override
    public List<CityType> getAll() {
        List<City> cities = factory.getCityDao().findAll();
        List<CityType> cityTypes = new ArrayList<>(cities.size());
        if (!cities.isEmpty()) {
            for (City city : cities) {
                CityType cityType = new CityType();
                convertToPresentationModel(city, cityType);
                cityTypes.add(cityType);
            }
        }
        return cityTypes;
    }

    @Override
    public CityType delete(Integer id) {
        City city = factory.getCityDao().delete(id);
        CityType cityType = new CityType();
        if (null != city) {
            convertToPresentationModel(city, cityType);
        }
        return cityType;
    }
}
