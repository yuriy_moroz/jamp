package com.epam.mentorship.persistence.dao.impl;

import com.epam.mentorship.persistence.dao.WorkPlaceDao;
import com.epam.mentorship.persistence.WorkPlaceDomain;

import java.util.ArrayList;
import java.util.List;

public class WorkPlaceDaoImpl implements WorkPlaceDao {

    private DomainStorage storage;

    public WorkPlaceDaoImpl() {
        storage = DomainStorage.getInstance();
    }

    @Override
    public WorkPlaceDomain create(WorkPlaceDomain entity) {
        Integer id = storage.getWorkPlaceCounter().incrementAndGet();
        entity.setNumber(id);
        return storage.getWorkPlaceMap().put(id, entity);
    }

    @Override
    public WorkPlaceDomain update(WorkPlaceDomain entity) {
        return storage.getWorkPlaceMap().put(entity.getNumber(), entity);
    }

    @Override
    public WorkPlaceDomain findById(Integer id) {
        return storage.getWorkPlaceMap().get(id);
    }

    @Override
    public List<WorkPlaceDomain> findAll() {
        return new ArrayList<>(storage.getWorkPlaceMap().values());
    }

    @Override
    public WorkPlaceDomain delete(Integer id) {
        return storage.getWorkPlaceMap().remove(id);
    }
}
