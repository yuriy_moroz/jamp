package com.epam.mentorship.persistence;

import java.util.List;

public class Country {

    private String name;
    private List<Locale> locales;
    private String isoCode;

    public Country(String name, List<Locale> locales, String isoCode) {
        this.name = name;
        this.locales = locales;
        this.isoCode = isoCode;
    }

    public Country() {
    }

    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public List<Locale> getLocales() {
        return locales;
    }

    public void setLocales(List<Locale> locales) {
        this.locales = locales;
    }

    public String getIsoCode() {
        return isoCode;
    }

    public void setIsoCode(String value) {
        this.isoCode = value;
    }

}
