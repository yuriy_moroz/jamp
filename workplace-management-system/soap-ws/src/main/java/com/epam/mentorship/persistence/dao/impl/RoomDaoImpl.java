package com.epam.mentorship.persistence.dao.impl;

import com.epam.mentorship.persistence.dao.RoomDao;
import com.epam.mentorship.persistence.Room;

import java.util.ArrayList;
import java.util.List;

public class RoomDaoImpl implements RoomDao {

    private DomainStorage storage;

    public RoomDaoImpl() {
        storage = DomainStorage.getInstance();
    }

    @Override
    public Room create(Room entity) {
        Integer id = storage.getRoomCounter().incrementAndGet();
        entity.setId(id);
        return storage.getRoomMap().put(id, entity);
    }

    @Override
    public Room update(Room entity) {
        return storage.getRoomMap().put(entity.getId(), entity);
    }

    @Override
    public Room findById(Integer id) {
        return storage.getRoomMap().get(id);
    }

    @Override
    public List<Room> findAll() {
        return new ArrayList<>(storage.getRoomMap().values());
    }

    @Override
    public Room delete(Integer id) {
        return storage.getRoomMap().remove(id);
    }
}
