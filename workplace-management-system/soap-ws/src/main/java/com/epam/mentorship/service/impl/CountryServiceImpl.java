package com.epam.mentorship.service.impl;

import com.epam.mentorship.persistence.dao.DomainFactory;
import com.epam.mentorship.persistence.dao.impl.DomainFactoryImpl;
import com.epam.mentorship.persistence.Country;
import com.epam.mentorship.persistence.Locale;
import com.epam.mentorship.presentation.domain.CountryType;
import com.epam.mentorship.presentation.domain.LocaleType;
import com.epam.mentorship.presentation.domain.LocalesType;
import com.epam.mentorship.presentation.exception.CountryNotFoundException;
import com.epam.mentorship.service.CountryService;

import java.util.ArrayList;
import java.util.List;

public class CountryServiceImpl implements CountryService {

    private DomainFactory factory;

    public CountryServiceImpl() {
        factory = new DomainFactoryImpl();
    }

    @Override
    public void convertToPersistenceModel(CountryType from, Country to) {
        to.setName(from.getName());
        to.setIsoCode(from.getIsoCode());
        to.setLocales(convertToLocaleList(from.getLocales()));
    }

    @Override
    public void convertToPresentationModel(Country from, CountryType to) {
        to.setName(from.getName());
        to.setLocales(convertToLocalesType(from.getLocales()));
        to.setIsoCode(from.getIsoCode());
    }

    @Override
    public CountryType add(CountryType entity) {
        Country country = new Country();
        convertToPersistenceModel(entity, country);
        factory.getCountryDao().create(country);
        return entity;
    }

    @Override
    public CountryType update(CountryType entity) throws CountryNotFoundException {
        Country country = factory.getCountryDao().findById(entity.getName());
        if (null == country) {
            throw new CountryNotFoundException(entity.getName());
        }
        convertToPersistenceModel(entity, country);
        factory.getCountryDao().update(country);
        return entity;
    }

    @Override
    public CountryType get(String id) throws CountryNotFoundException {
        Country country = factory.getCountryDao().findById(id);
        if (null == country) {
            throw new CountryNotFoundException(id);
        }
        CountryType countryType = new CountryType();
        convertToPresentationModel(country, countryType);
        return countryType;
    }

    @Override
    public List<CountryType> getAll() {
        List<Country> countries = factory.getCountryDao().findAll();
        List<CountryType> countryTypeList = new ArrayList<>(countries.size());
        for (Country country : countries) {
            CountryType countryType = new CountryType();
            convertToPresentationModel(country, countryType);
            countryTypeList.add(countryType);
        }
        return countryTypeList;
    }

    @Override
    public CountryType delete(String id) {
        Country country = factory.getCountryDao().delete(id);
        CountryType countryType = new CountryType();
        if (null == country) {
            return countryType;
        }
        convertToPresentationModel(country, countryType);
        return countryType;
    }


    private List<Locale> convertToLocaleList(LocalesType localesType) {
        List<Locale> locales = new ArrayList<>();
        for (LocaleType localeType : localesType.getLocale()) {
            locales.add(Locale.fromValue(localeType.value()));
        }
        return locales;
    }

    private LocalesType convertToLocalesType(List<Locale> localeList) {
        LocalesType localesType = new LocalesType();
        for (Locale locale : localeList) {
            localesType.getLocale().add(LocaleType.fromValue(locale.value()));
        }
        return localesType;
    }
}
