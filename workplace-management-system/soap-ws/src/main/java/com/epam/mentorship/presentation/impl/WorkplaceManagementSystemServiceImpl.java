package com.epam.mentorship.presentation.impl;

import com.epam.mentorship.presentation.exception.*;
import com.epam.mentorship.presentation.domain.*;
import com.epam.mentorship.presentation.WorkplaceManagementSystemService;
import com.epam.mentorship.service.*;
import com.epam.mentorship.service.impl.*;

import javax.jws.HandlerChain;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "com.epam.mentorship.presentation.WorkplaceManagementSystemService")
@HandlerChain(file="handler-chain.xml")
public class WorkplaceManagementSystemServiceImpl implements WorkplaceManagementSystemService {

    private CountryService countryService;
    private CityService cityService;
    private AddressService addressService;
    private EmployeeService employeeService;
    private CompanyService companyService;
    private OfficeService officeService;
    private AreaService areaService;
    private RoomService roomService;
    private WorkPlaceService workPlaceService;

    public WorkplaceManagementSystemServiceImpl() {
        countryService = new CountryServiceImpl();
        cityService = new CityServiceImpl();
        addressService = new AddressServiceImpl();
        employeeService = new EmployeeServiceImpl();
        companyService = new CompanyServiceImpl();
        officeService = new OfficeServiceImpl();
        areaService = new AreaServiceImpl();
        roomService = new RoomServiceImpl();
        workPlaceService = new WorkPlaceServiceImpl();
    }

    @Override
    public CountryType addCountry(@WebParam CountryType countryType) {
        return countryService.add(countryType);
    }

    @Override
    public CountryType updateCountry(@WebParam CountryType countryType) throws CountryNotFoundException {
        return countryService.update(countryType);
    }

    @Override
    public CountryType getCountry(@WebParam(name = "Name") String name) throws CountryNotFoundException {
        return countryService.get(name);
    }

    @Override
    public List<CountryType> getAllCountries() {
        return countryService.getAll();
    }

    @Override
    public CountryType deleteCountry(@WebParam String name) {
        return countryService.delete(name);
    }

    @Override
    public CityType addCity(CityType cityType) {
        return cityService.add(cityType);
    }

    @Override
    public CityType updateCity(CityType cityType) throws CityNotFoundException {
        return cityService.update(cityType);
    }

    @Override
    public CityType getCity(Integer id) throws CityNotFoundException {
        return cityService.get(id);
    }

    @Override
    public List<CityType> getAllCities() {
        return cityService.getAll();
    }

    @Override
    public CityType deleteCity(Integer id) {
        return cityService.delete(id);
    }

    @Override
    public AddressType addAddressType(AddressType addressType) {
        return addressService.add(addressType);
    }

    @Override
    public AddressType updateAddress(AddressType addressType) throws AddressNotFoundException {
        return addressService.update(addressType);
    }

    @Override
    public AddressType getAddress(Integer id) throws AddressNotFoundException {
        return addressService.get(id);
    }

    @Override
    public List<AddressType> getAllAddresses() {
        return addressService.getAll();
    }

    @Override
    public AddressType deleteAddress(Integer id) {
        return addressService.delete(id);
    }

    @Override
    public EmployeeType addEmployee(EmployeeType employeeType) {
        return employeeService.add(employeeType);
    }

    @Override
    public EmployeeType getEmployee(Integer id) throws EmployeeNotFoundException {
        return employeeService.get(id);
    }

    @Override
    public List<EmployeeType> getAllEmployees() {
        return employeeService.getAll();
    }

    @Override
    public EmployeeType updateEmployee(EmployeeType employeeType) throws EmployeeNotFoundException {
        return employeeService.update(employeeType);
    }

    @Override
    public EmployeeType deleteEmployee(Integer id) {
        return employeeService.delete(id);
    }

    @Override
    public CompanyType addCompany(CompanyType companyType) {
        return companyService.add(companyType);
    }

    @Override
    public CompanyType updateCompany(CompanyType companyType) throws CompanyNotFoundException {
        return companyService.update(companyType);
    }

    @Override
    public CompanyType getCompany(Integer id) throws CompanyNotFoundException {
        return companyService.get(id);
    }

    @Override
    public List<CompanyType> getAllCompanies() {
        return companyService.getAll();
    }

    @Override
    public CompanyType deleteCompany(Integer id) {
        return companyService.delete(id);
    }

    @Override
    public OfficeType addOffice(OfficeType officeType) {
        return officeService.add(officeType);
    }

    @Override
    public OfficeType updateOffice(OfficeType officeType) throws OfficeNotFoundException {
        return officeService.update(officeType);
    }

    @Override
    public OfficeType getOffice(Integer id) throws OfficeNotFoundException {
        return officeService.get(id);
    }

    @Override
    public List<OfficeType> getAllOffices() {
        return officeService.getAll();
    }

    @Override
    public OfficeType deleteOffice(Integer id) {
        return officeService.delete(id);
    }

    @Override
    public AreaType addArea(AreaType areaType) {
        return areaService.add(areaType);
    }

    @Override
    public AreaType updateArea(AreaType areaType) throws AreaNotFoundException {
        return areaService.update(areaType);
    }

    @Override
    public AreaType getArea(Integer id) throws AreaNotFoundException {
        return areaService.get(id);
    }

    @Override
    public List<AreaType> getAllAreas() {
        return areaService.getAll();
    }

    @Override
    public AreaType deleteArea(Integer id) {
        return areaService.delete(id);
    }

    @Override
    public RoomType addRoom(RoomType roomType) {
        return roomService.add(roomType);
    }

    @Override
    public RoomType updateRoom(RoomType roomType) throws RoomNotFoundException {
        return roomService.update(roomType);
    }

    @Override
    public RoomType getRoom(Integer id) throws RoomNotFoundException {
        return roomService.get(id);
    }

    @Override
    public List<RoomType> getAllRooms() {
        return roomService.getAll();
    }

    @Override
    public RoomType deleteRoom(Integer id) {
        return roomService.delete(id);
    }

    @Override
    public WorkPlace addWorkPlace(WorkPlace workPlace) {
        return workPlaceService.add(workPlace);
    }

    @Override
    public WorkPlace updateWorkPlace(WorkPlace workPlace) throws WorkPlaceNotFoundException {
        return workPlaceService.update(workPlace);
    }

    @Override
    public WorkPlace getWorkPlace(Integer number) throws WorkPlaceNotFoundException {
        return workPlaceService.get(number);
    }

    @Override
    public List<WorkPlace> getAllWorkPlaces() {
        return workPlaceService.getAll();
    }

    @Override
    public WorkPlace deleteWorkPlace(Integer number) {
        return workPlaceService.delete(number);
    }

}
