package com.epam.mentorship.persistence.dao.impl;

import com.epam.mentorship.persistence.dao.AddressDao;
import com.epam.mentorship.persistence.Address;

import java.util.ArrayList;
import java.util.List;

public class AddressDaoImpl implements AddressDao {

    private DomainStorage storage;

    public AddressDaoImpl() {
        storage = DomainStorage.getInstance();
    }

    @Override
    public Address create(Address entity) {
        Integer id = storage.getAddressCounter().incrementAndGet();
        entity.setId(id);
        return storage.getAddressMap().put(id, entity);
    }

    @Override
    public Address update(Address entity) {
        return storage.getAddressMap().put(entity.getId(), entity);
    }

    @Override
    public Address findById(Integer id) {
        return storage.getAddressMap().get(id);
    }

    @Override
    public List<Address> findAll() {
        return new ArrayList<>(storage.getAddressMap().values());
    }

    @Override
    public Address delete(Integer id) {
        return storage.getAddressMap().remove(id);
    }
}
