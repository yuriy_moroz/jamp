package com.epam.mentorship.persistence;

public enum PrimarySkill {

    JAVA("JAVA"),
    DBA("DBA"),
    JAVASCRIPT("JAVASCRIPT"),
    NET(".NET");

    private final String value;

    PrimarySkill(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PrimarySkill fromValue(String v) {
        for (PrimarySkill c: PrimarySkill.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
