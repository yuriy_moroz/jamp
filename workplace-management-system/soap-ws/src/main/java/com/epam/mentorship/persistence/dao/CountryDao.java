package com.epam.mentorship.persistence.dao;

import com.epam.mentorship.persistence.Country;

public interface CountryDao extends CrudDao<String, Country> {}
