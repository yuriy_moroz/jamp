package com.epam.mentorship.service;

import com.epam.mentorship.persistence.Office;
import com.epam.mentorship.presentation.domain.OfficeType;
import com.epam.mentorship.presentation.exception.OfficeNotFoundException;

public interface OfficeService
        extends CrudService<Integer, OfficeType, OfficeNotFoundException>,
        Converter<OfficeType, Office> {
}
