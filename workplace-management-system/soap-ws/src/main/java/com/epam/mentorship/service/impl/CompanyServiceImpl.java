package com.epam.mentorship.service.impl;

import com.epam.mentorship.persistence.dao.DomainFactory;
import com.epam.mentorship.persistence.dao.impl.DomainFactoryImpl;
import com.epam.mentorship.persistence.Company;
import com.epam.mentorship.presentation.domain.CompanyType;
import com.epam.mentorship.presentation.exception.CompanyNotFoundException;
import com.epam.mentorship.service.CompanyService;

import java.util.ArrayList;
import java.util.List;

public class CompanyServiceImpl implements CompanyService {

    private DomainFactory factory;

    public CompanyServiceImpl() {
        factory = new DomainFactoryImpl();
    }

    @Override
    public void convertToPersistenceModel(CompanyType from, Company to) {
        to.setName(from.getName());
        to.setHeadOffice(factory.getOfficeDao().findById(from.getHeadOffice()));
    }

    @Override
    public void convertToPresentationModel(Company from, CompanyType to) {
        to.setId(from.getId());
        to.setName(from.getName());
        to.setHeadOffice(from.getHeadOffice() == null ? null : from.getHeadOffice().getId());
    }

    @Override
    public CompanyType add(CompanyType entity) {
        Company company = new Company();
        convertToPersistenceModel(entity, company);
        factory.getCompanyDao().create(company);
        entity.setId(company.getId());
        return entity;
    }

    @Override
    public CompanyType update(CompanyType entity) throws CompanyNotFoundException {
        Company company = factory.getCompanyDao().findById(entity.getId());
        if (null == company) {
            throw new CompanyNotFoundException(String.valueOf(entity.getId()));
        }
        convertToPersistenceModel(entity, company);
        factory.getCompanyDao().update(company);
        return entity;
    }

    @Override
    public CompanyType get(Integer id) throws CompanyNotFoundException {
        Company company = factory.getCompanyDao().findById(id);
        if (null == company) {
            throw new CompanyNotFoundException(id.toString());
        }
        CompanyType companyType = new CompanyType();
        convertToPresentationModel(company, companyType);
        return companyType;
    }

    @Override
    public List<CompanyType> getAll() {
        List<Company> companies = factory.getCompanyDao().findAll();
        List<CompanyType> companyTypes = new ArrayList<>(companies.size());
        if (!companies.isEmpty()) {
            for (Company company : companies) {
                CompanyType companyType = new CompanyType();
                convertToPresentationModel(company, companyType);
                companyTypes.add(companyType);
            }
        }
        return companyTypes;
    }

    @Override
    public CompanyType delete(Integer id) {
        Company company = factory.getCompanyDao().delete(id);
        CompanyType companyType = new CompanyType();
        if (null == company) {
            return companyType;
        }
        convertToPresentationModel(company, companyType);
        return companyType;
    }
}
