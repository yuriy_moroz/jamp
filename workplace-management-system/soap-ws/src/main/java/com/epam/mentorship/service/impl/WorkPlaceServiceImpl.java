package com.epam.mentorship.service.impl;

import com.epam.mentorship.persistence.dao.DomainFactory;
import com.epam.mentorship.persistence.dao.impl.DomainFactoryImpl;
import com.epam.mentorship.persistence.WorkPlaceDomain;
import com.epam.mentorship.presentation.domain.WorkPlace;
import com.epam.mentorship.presentation.exception.WorkPlaceNotFoundException;
import com.epam.mentorship.service.WorkPlaceService;

import java.util.ArrayList;
import java.util.List;

public class WorkPlaceServiceImpl implements WorkPlaceService {

    private DomainFactory factory;

    public WorkPlaceServiceImpl() {
        factory = new DomainFactoryImpl();
    }

    @Override
    public void convertToPersistenceModel(WorkPlace from, WorkPlaceDomain to) {
        to.setNumber(from.getNumber());
        to.setEmployee(factory.getEmployeeDao().findById(from.getEmployeeId()));
        to.setRoom(factory.getRoomDao().findById(from.getRoomId()));
        to.setCoordinate(new WorkPlaceDomain.Coordinate(
                from.getCoordinate().getRow(), from.getCoordinate().getColumn()));
    }

    @Override
    public void convertToPresentationModel(WorkPlaceDomain from, WorkPlace to) {
        to.setNumber(from.getNumber());
        to.setEmployeeId(from.getEmployee().getId());
        to.setRoomId(from.getRoom().getId());
        WorkPlace.Coordinate coordinate = new WorkPlace.Coordinate();
        coordinate.setColumn(from.getCoordinate().getColumn());
        coordinate.setRow(from.getCoordinate().getRow());
        to.setCoordinate(coordinate);
    }

    @Override
    public WorkPlace add(WorkPlace entity) {
        WorkPlaceDomain workPlaceDomaim = new WorkPlaceDomain();
        convertToPersistenceModel(entity, workPlaceDomaim);
        factory.getWorkPlaceDao().create(workPlaceDomaim);
        entity.setNumber(workPlaceDomaim.getNumber());
        return entity;
    }

    @Override
    public WorkPlace update(WorkPlace entity) throws WorkPlaceNotFoundException {
        WorkPlaceDomain workPlaceDomain = factory.getWorkPlaceDao().findById(entity.getNumber());
        if (null == workPlaceDomain) {
            throw new WorkPlaceNotFoundException(String.valueOf(entity.getNumber()));
        }
        convertToPersistenceModel(entity, workPlaceDomain);
        factory.getWorkPlaceDao().update(workPlaceDomain);
        return entity;
    }

    @Override
    public WorkPlace get(Integer id) throws WorkPlaceNotFoundException {
        WorkPlaceDomain workPlaceDomain = factory.getWorkPlaceDao().findById(id);
        if (null == workPlaceDomain) {
            throw new WorkPlaceNotFoundException(String.valueOf(id.toString()));
        }
        WorkPlace workPlace = new WorkPlace();
        convertToPresentationModel(workPlaceDomain, workPlace);
        return workPlace;
    }

    @Override
    public List<WorkPlace> getAll() {
        List<WorkPlaceDomain> workPlaceDomains = factory.getWorkPlaceDao().findAll();
        List<WorkPlace> workPlaces = new ArrayList<>(workPlaceDomains.size());
        if (!workPlaceDomains.isEmpty()) {
            for (WorkPlaceDomain workPlaceDomain : workPlaceDomains) {
                WorkPlace workPlace = new WorkPlace();
                convertToPresentationModel(workPlaceDomain, workPlace);
                workPlaces.add(workPlace);
            }
        }
        return workPlaces;
    }

    @Override
    public WorkPlace delete(Integer id) {
        WorkPlaceDomain workPlaceDomain = factory.getWorkPlaceDao().delete(id);
        WorkPlace workPlace = new WorkPlace();
        if (null != workPlaceDomain) {
            convertToPresentationModel(workPlaceDomain, workPlace);
        }
        return workPlace;
    }
}
