package com.epam.mentorship.presentation.exception;

public class RoomNotFoundException extends Exception {
    public RoomNotFoundException(String id) {
        super("Room isn't found for id "+id);
    }
}
