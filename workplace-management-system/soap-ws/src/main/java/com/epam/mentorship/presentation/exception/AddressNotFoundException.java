package com.epam.mentorship.presentation.exception;

public class AddressNotFoundException extends Exception {
    public AddressNotFoundException(String id) {
        super("Address isn't found for id "+id);
    }
}
