package com.epam.mentorship.persistence.dao;

import com.epam.mentorship.persistence.Office;

public interface OfficeDao extends CrudDao<Integer, Office> {
}
