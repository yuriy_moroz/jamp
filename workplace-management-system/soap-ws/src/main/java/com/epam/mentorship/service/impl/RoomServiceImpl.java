package com.epam.mentorship.service.impl;

import com.epam.mentorship.persistence.dao.DomainFactory;
import com.epam.mentorship.persistence.dao.impl.DomainFactoryImpl;
import com.epam.mentorship.persistence.Room;
import com.epam.mentorship.presentation.domain.RoomType;
import com.epam.mentorship.presentation.exception.RoomNotFoundException;
import com.epam.mentorship.service.RoomService;

import java.util.ArrayList;
import java.util.List;

public class RoomServiceImpl implements RoomService {

    private DomainFactory factory;

    public RoomServiceImpl() {
        factory = new DomainFactoryImpl();
    }

    @Override
    public void convertToPersistenceModel(RoomType from, Room to) {
        to.setArea(factory.getAreaDao().findById(from.getAreaId()));
        to.setColumnCount(from.getColumnCount());
        to.setRowCount(from.getRowCount());
        to.setNumber(from.getNumber());
        to.setWorkPlaceCount(from.getWorkPlaceCount());
    }

    @Override
    public void convertToPresentationModel(Room from, RoomType to) {
        to.setId(from.getId());
        to.setWorkPlaceCount(from.getWorkPlaceCount());
        to.setAreaId(from.getArea().getId());
        to.setNumber(from.getNumber());
        to.setRowCount(from.getRowCount());
        to.setColumnCount(from.getColumnCount());
    }

    @Override
    public RoomType add(RoomType entity) {
        Room room = new Room();
        convertToPersistenceModel(entity, room);
        factory.getRoomDao().create(room);
        entity.setId(room.getId());
        return entity;
    }

    @Override
    public RoomType update(RoomType entity) throws RoomNotFoundException {
        Room room = factory.getRoomDao().findById(entity.getId());
        if (null == room) {
            throw new RoomNotFoundException(String.valueOf(entity.getId()));
        }
        convertToPersistenceModel(entity, room);
        factory.getRoomDao().update(room);
        return entity;
    }

    @Override
    public RoomType get(Integer id) throws RoomNotFoundException {
        Room room = factory.getRoomDao().findById(id);
        if (null == room) {
            throw new RoomNotFoundException(id.toString());
        }
        RoomType roomType = new RoomType();
        convertToPresentationModel(room, roomType);
        return roomType;
    }

    @Override
    public List<RoomType> getAll() {
        List<Room> rooms = factory.getRoomDao().findAll();
        List<RoomType> roomTypes = new ArrayList<>(rooms.size());
        if (!rooms.isEmpty()) {
            for (Room room : rooms) {
                RoomType roomType = new RoomType();
                convertToPresentationModel(room, roomType);
                roomTypes.add(roomType);
            }
        }
        return roomTypes;
    }

    @Override
    public RoomType delete(Integer id) {
        Room room = factory.getRoomDao().delete(id);
        RoomType roomType = new RoomType();
        if (null != room) {
            convertToPresentationModel(room, roomType);
        }
        return roomType;
    }
}
