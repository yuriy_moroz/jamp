package com.epam.mentorship.persistence.dao.impl;

import com.epam.mentorship.persistence.dao.CountryDao;
import com.epam.mentorship.persistence.Country;

import java.util.ArrayList;
import java.util.List;

public class CountryDaoImpl implements CountryDao {

    private DomainStorage storage;

    public CountryDaoImpl() {
        storage = DomainStorage.getInstance();
    }

    @Override
    public Country create(Country entity) {
        return storage.getCountryMap().put(entity.getName(), entity);
    }

    @Override
    public Country update(Country entity) {
        Country country = storage.getCountryMap().get(entity.getName());
        if (null != country) {
            storage.getCountryMap().put(entity.getName(), entity);
        }
        return entity;
    }

    @Override
    public Country findById(String id) {
        return storage.getCountryMap().get(id);
    }
    @Override
    public List<Country> findAll() {
        return new ArrayList<>(storage.getCountryMap().values());
    }

    @Override
    public Country delete(String id) {
        return storage.getCountryMap().remove(id);
    }
}
