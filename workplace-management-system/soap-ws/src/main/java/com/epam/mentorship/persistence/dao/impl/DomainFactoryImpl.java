package com.epam.mentorship.persistence.dao.impl;

import com.epam.mentorship.persistence.dao.AddressDao;
import com.epam.mentorship.persistence.dao.AreaDao;
import com.epam.mentorship.persistence.dao.CityDao;
import com.epam.mentorship.persistence.dao.CompanyDao;
import com.epam.mentorship.persistence.dao.CountryDao;
import com.epam.mentorship.persistence.dao.DomainFactory;
import com.epam.mentorship.persistence.dao.EmployeeDao;
import com.epam.mentorship.persistence.dao.OfficeDao;
import com.epam.mentorship.persistence.dao.RoomDao;
import com.epam.mentorship.persistence.dao.WorkPlaceDao;

public class DomainFactoryImpl implements DomainFactory {
    @Override
    public CountryDao getCountryDao() {
        return new CountryDaoImpl();
    }

    @Override
    public CityDao getCityDao() {
        return new CityDaoImpl();
    }

    @Override
    public AddressDao getAddressDao() {
        return new AddressDaoImpl();
    }

    @Override
    public EmployeeDao getEmployeeDao() {
        return new EmployeeDaoImpl();
    }

    @Override
    public CompanyDao getCompanyDao() {
        return new CompanyDaoImpl();
    }

    @Override
    public OfficeDao getOfficeDao() {
        return new OfficeDaoImpl();
    }

    @Override
    public AreaDao getAreaDao() {
        return new AreaDaoImpl();
    }

    @Override
    public RoomDao getRoomDao() {
        return new RoomDaoImpl();
    }

    @Override
    public WorkPlaceDao getWorkPlaceDao() {
        return new WorkPlaceDaoImpl();
    }
}
