package com.epam.mentorship.persistence;

public class Office {

    private Integer id;
    private String name;
    private Company company;
    private Address address;
    private Type type;
    private int employeeCount;
    private Integer areaCount;
    private int square;
    private Integer floorCount;

    public Office() {
    }

    public Office(Integer id, String name, Company company,
                  Address address, Type type,
                  int employeeCount, Integer areaCount,
                  int square, Integer floorCount) {
        this.id = id;
        this.name = name;
        this.company = company;
        this.address = address;
        this.type = type;
        this.employeeCount = employeeCount;
        this.areaCount = areaCount;
        this.square = square;
        this.floorCount = floorCount;
    }
    public static enum Type {

        CELLULAR,
        COMBINATION,
        TEAM,
        OPEN_SPACE;

        public String value() {
            return name();
        }

        public static Type fromValue(String v) {
            return valueOf(v);
        }

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public int getEmployeeCount() {
        return employeeCount;
    }

    public void setEmployeeCount(int employeeCount) {
        this.employeeCount = employeeCount;
    }

    public Integer getAreaCount() {
        return areaCount;
    }

    public void setAreaCount(Integer areaCount) {
        this.areaCount = areaCount;
    }

    public int getSquare() {
        return square;
    }

    public void setSquare(int square) {
        this.square = square;
    }

    public Integer getFloorCount() {
        return floorCount;
    }

    public void setFloorCount(Integer floorCount) {
        this.floorCount = floorCount;
    }
}
