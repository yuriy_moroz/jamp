package com.epam.mentorship.service;

import java.util.List;

public interface CrudService<I, P, E extends Throwable> {

    P add(P entity);

    P update(P entity) throws E;

    P get(I id) throws E;

    List<P> getAll();

    P delete(I id);
}
