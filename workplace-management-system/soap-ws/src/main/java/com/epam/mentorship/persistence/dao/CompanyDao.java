package com.epam.mentorship.persistence.dao;

import com.epam.mentorship.persistence.Company;

public interface CompanyDao extends CrudDao<Integer, Company> {
}
