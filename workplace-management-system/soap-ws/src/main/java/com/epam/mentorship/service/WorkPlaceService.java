package com.epam.mentorship.service;

import com.epam.mentorship.persistence.WorkPlaceDomain;
import com.epam.mentorship.presentation.domain.WorkPlace;
import com.epam.mentorship.presentation.exception.WorkPlaceNotFoundException;

public interface WorkPlaceService
        extends CrudService<Integer, WorkPlace, WorkPlaceNotFoundException>,
        Converter<WorkPlace, WorkPlaceDomain> {
}
