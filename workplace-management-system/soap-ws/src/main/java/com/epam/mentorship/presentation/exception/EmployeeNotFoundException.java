package com.epam.mentorship.presentation.exception;

public class EmployeeNotFoundException extends Exception {
    public EmployeeNotFoundException(String id) {
        super("Employee isn't found for id "+id);
    }
}
