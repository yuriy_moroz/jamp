package com.epam.mentorship.persistence;

public class Employee {

    private Integer id;
    private String name;
    private PrimarySkill primarySkill;
    private String position;

    public Employee(Integer id, String name, PrimarySkill primarySkill, String position) {
        this.id = id;
        this.name = name;
        this.primarySkill = primarySkill;
        this.position = position;
    }

    public Employee() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public PrimarySkill getPrimarySkill() {
        return primarySkill;
    }

    public void setPrimarySkill(PrimarySkill value) {
        this.primarySkill = value;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String value) {
        this.position = value;
    }

}
