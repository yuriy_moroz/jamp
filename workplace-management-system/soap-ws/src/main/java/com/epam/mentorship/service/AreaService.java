package com.epam.mentorship.service;

import com.epam.mentorship.persistence.Area;
import com.epam.mentorship.presentation.domain.AreaType;
import com.epam.mentorship.presentation.exception.AreaNotFoundException;

public interface AreaService
        extends CrudService<Integer, AreaType, AreaNotFoundException>,
        Converter<AreaType, Area> {
}
