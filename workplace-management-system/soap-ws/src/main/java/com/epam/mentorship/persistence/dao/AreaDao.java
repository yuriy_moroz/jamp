package com.epam.mentorship.persistence.dao;

import com.epam.mentorship.persistence.Area;

public interface AreaDao extends CrudDao<Integer, Area> {
}
