package com.epam.mentorship.service;

import com.epam.mentorship.persistence.Country;
import com.epam.mentorship.presentation.exception.CountryNotFoundException;
import com.epam.mentorship.presentation.domain.CountryType;

public interface CountryService
        extends CrudService<String, CountryType, CountryNotFoundException>,
        Converter<CountryType, Country> {
}
