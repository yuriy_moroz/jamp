package com.epam.mentorship.presentation.exception;

public class WorkPlaceNotFoundException extends Exception {
    public WorkPlaceNotFoundException(String id) {
        super("WorkPlace isn't found for id "+id);
    }
}
