package com.epam.mentorship.persistence;

public class Room {

    private Integer id;
    private Integer number;
    private Area area;
    private Integer workPlaceCount;
    private Integer rowCount;
    private Integer columnCount;

    public Room(Integer id, Integer number, Area area, Integer workPlaceCount, Integer rowCount, Integer columnCount) {
        this.id = id;
        this.number = number;
        this.area = area;
        this.workPlaceCount = workPlaceCount;
        this.rowCount = rowCount;
        this.columnCount = columnCount;
    }

    public Room() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public Integer getWorkPlaceCount() {
        return workPlaceCount;
    }

    public void setWorkPlaceCount(Integer workPlaceCount) {
        this.workPlaceCount = workPlaceCount;
    }

    public Integer getRowCount() {
        return rowCount;
    }

    public void setRowCount(Integer rowCount) {
        this.rowCount = rowCount;
    }

    public Integer getColumnCount() {
        return columnCount;
    }

    public void setColumnCount(Integer columnCount) {
        this.columnCount = columnCount;
    }
}
