package com.epam.mentorship.service;

import com.epam.mentorship.persistence.Company;
import com.epam.mentorship.presentation.domain.CompanyType;
import com.epam.mentorship.presentation.exception.CompanyNotFoundException;

public interface CompanyService
        extends CrudService<Integer, CompanyType, CompanyNotFoundException>,
        Converter<CompanyType, Company> {
}
