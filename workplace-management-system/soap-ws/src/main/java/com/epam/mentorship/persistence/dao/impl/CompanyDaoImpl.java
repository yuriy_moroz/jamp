package com.epam.mentorship.persistence.dao.impl;

import com.epam.mentorship.persistence.dao.CompanyDao;
import com.epam.mentorship.persistence.Company;

import java.util.ArrayList;
import java.util.List;

public class CompanyDaoImpl implements CompanyDao {

    private DomainStorage storage;

    public CompanyDaoImpl() {
        storage = DomainStorage.getInstance();
    }

    @Override
    public Company create(Company entity) {
        Integer id = storage.getCompanyCounter().incrementAndGet();
        entity.setId(id);
        return storage.getCompanyMap().put(id, entity);
    }

    @Override
    public Company update(Company entity) {
        return storage.getCompanyMap().put(entity.getId(), entity);
    }

    @Override
    public Company findById(Integer id) {
        return storage.getCompanyMap().get(id);
    }

    @Override
    public List<Company> findAll() {
        return new ArrayList<>(storage.getCompanyMap().values());
    }

    @Override
    public Company delete(Integer id) {
        return storage.getCompanyMap().remove(id);
    }
}
