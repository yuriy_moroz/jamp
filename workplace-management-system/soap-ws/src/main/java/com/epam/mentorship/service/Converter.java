package com.epam.mentorship.service;

public interface Converter<P, D> {

    void convertToPersistenceModel(P from, D to);

    void convertToPresentationModel(D from, P to);
}
