package com.epam.mentorship.presentation.handler;

import org.w3c.dom.NamedNodeMap;

import javax.xml.bind.Element;
import javax.xml.namespace.QName;
import javax.xml.soap.Node;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import javax.xml.ws.soap.SOAPFaultException;
import java.util.Iterator;
import java.util.Set;

public class ValidationHandler implements SOAPHandler<SOAPMessageContext> {

    @Override
    public Set<QName> getHeaders() {
        return null;
    }

    @Override
    public boolean handleMessage(SOAPMessageContext context) {
        Boolean outbound = (Boolean)context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
        if (outbound) {
            return false;
        }
        SOAPMessage soapMessage = context.getMessage();
        SOAPBody body= null;
        try {
            body = soapMessage.getSOAPPart().getEnvelope().getBody();
        } catch (SOAPException e) {
            e.printStackTrace();
        }
        if (body != null) {
            Iterator<Node> nodes = body.getChildElements();
            Node node;
            while (nodes.hasNext()) {
                node=nodes.next();
                if (node instanceof SOAPBodyElement) {
                    String operationName = node.getLocalName();
                    if ("addCountry".equals(operationName)) {
                        Iterator<Node> childElements = ((SOAPBodyElement) node).getChildElements();
                        Node childElement;
                        while (childElements.hasNext()) {
                            childElement = childElements.next();
                            if (node instanceof SOAPElement &&
                                    "country".equals(childElement.getLocalName())) {
                                NamedNodeMap attributes = childElement.getAttributes();
                                String isoCode = attributes.getNamedItem("isoCode").getNodeValue();
                                if (!isoCode.contains("ISO")) {
                                    try {
                                        SOAPBody soapBody = soapMessage.getSOAPPart().getEnvelope().getBody();
                                        SOAPFault soapFault = soapBody.addFault();
                                        soapFault.setFaultString("Invalid isoCode");
                                        throw new SOAPFaultException(soapFault);
                                    } catch (SOAPException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        return false;
    }

    @Override
    public void close(MessageContext context) {

    }
}
