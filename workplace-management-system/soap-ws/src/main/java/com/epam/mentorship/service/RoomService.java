package com.epam.mentorship.service;

import com.epam.mentorship.persistence.Room;
import com.epam.mentorship.presentation.domain.RoomType;
import com.epam.mentorship.presentation.exception.RoomNotFoundException;

public interface RoomService
        extends CrudService<Integer, RoomType, RoomNotFoundException>,
        Converter<RoomType, Room> {
}
