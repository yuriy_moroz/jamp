package com.epam.mentorship.persistence.dao;

import com.epam.mentorship.persistence.City;

public interface CityDao extends CrudDao<Integer, City> {
}
