package com.epam.mentorship.persistence.dao;

import com.epam.mentorship.persistence.Room;

public interface RoomDao extends CrudDao<Integer, Room> {
}
