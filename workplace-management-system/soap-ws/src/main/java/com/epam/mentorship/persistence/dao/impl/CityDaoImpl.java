package com.epam.mentorship.persistence.dao.impl;

import com.epam.mentorship.persistence.dao.CityDao;
import com.epam.mentorship.persistence.City;

import java.util.ArrayList;
import java.util.List;

public class CityDaoImpl implements CityDao {

    private DomainStorage storage;

    public CityDaoImpl() {
        storage = DomainStorage.getInstance();
    }

    @Override
    public City create(City entity) {
        Integer id = storage.getCityCounter().incrementAndGet();
        entity.setId(id);
        return storage.getCityMap().put(id, entity);
    }

    @Override
    public City update(City entity) {
        storage.getCityMap().put(entity.getId(), entity);
        return entity;
    }

    @Override
    public City findById(Integer id) {
        return storage.getCityMap().get(id);
    }

    @Override
    public List<City> findAll() {
        return new ArrayList<>(storage.getCityMap().values());
    }

    @Override
    public City delete(Integer id) {
        return storage.getCityMap().remove(id);
    }
}
