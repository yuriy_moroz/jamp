package com.epam.mentorship.persistence;

public class Company {

    private Integer id;
    private String name;
    private Office headOffice;

    public Company(Integer id, String name, Office headOffice) {
        this.id = id;
        this.name = name;
        this.headOffice = headOffice;
    }

    public Company() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public Office getHeadOffice() {
        return headOffice;
    }

    public void setHeadOffice(Office value) {
        this.headOffice = value;
    }

}
