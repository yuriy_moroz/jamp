package com.epam.mentorship.persistence.dao.impl;

import com.epam.mentorship.persistence.dao.AreaDao;
import com.epam.mentorship.persistence.Area;

import java.util.ArrayList;
import java.util.List;

public class AreaDaoImpl implements AreaDao {

    private DomainStorage storage;

    public AreaDaoImpl() {
        storage = DomainStorage.getInstance();
    }

    @Override
    public Area create(Area entity) {
        return storage.getAreaMap().put(entity.getId(), entity);
    }

    @Override
    public Area update(Area entity) {
        return storage.getAreaMap().put(entity.getId(), entity);
    }

    @Override
    public Area findById(Integer id) {
        return storage.getAreaMap().get(id);
    }

    @Override
    public List<Area> findAll() {
        return new ArrayList<>(storage.getAreaMap().values());
    }

    @Override
    public Area delete(Integer id) {
        return storage.getAreaMap().remove(id);
    }
}
