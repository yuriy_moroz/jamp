package com.epam.mentorship.service.impl;

import com.epam.mentorship.persistence.dao.DomainFactory;
import com.epam.mentorship.persistence.dao.impl.DomainFactoryImpl;
import com.epam.mentorship.persistence.Office;
import com.epam.mentorship.presentation.domain.OfficeSimpleType;
import com.epam.mentorship.presentation.domain.OfficeType;
import com.epam.mentorship.presentation.exception.OfficeNotFoundException;
import com.epam.mentorship.service.OfficeService;

import java.util.ArrayList;
import java.util.List;

public class OfficeServiceImpl implements OfficeService {

    private DomainFactory factory;

    public OfficeServiceImpl() {
        factory = new DomainFactoryImpl();
    }

    @Override
    public void convertToPersistenceModel(OfficeType from, Office to) {
        to.setType(Office.Type.valueOf(from.getType().value()));
        to.setName(from.getName());
        to.setSquare(from.getSquare());
        to.setFloorCount(from.getFloorCount());
        to.setAreaCount(from.getAreaCount());
        to.setAddress(factory.getAddressDao().findById(from.getAddressId()));
        to.setCompany(factory.getCompanyDao().findById(from.getCompanyId()));
        to.setEmployeeCount(from.getEmployeeCount());
    }

    @Override
    public void convertToPresentationModel(Office from, OfficeType to) {
        to.setId(from.getId());
        to.setName(from.getName());
        to.setEmployeeCount(from.getEmployeeCount());
        to.setAreaCount(from.getAreaCount());
        to.setFloorCount(from.getFloorCount());
        to.setSquare(from.getSquare());
        to.setAddressId(from.getAddress().getId());
        to.setCompanyId(from.getCompany().getId());
        to.setType(OfficeSimpleType.fromValue(from.getType().value()));
    }

    @Override
    public OfficeType add(OfficeType entity) {
        Office office = new Office();
        convertToPersistenceModel(entity, office);
        factory.getOfficeDao().create(office);
        office.setId(office.getId());
        return entity;
    }

    @Override
    public OfficeType update(OfficeType entity) throws OfficeNotFoundException {
        Office office = factory.getOfficeDao().findById(entity.getId());
        if (null == office) {
            throw new OfficeNotFoundException(String.valueOf(entity.getId()));
        }
        convertToPersistenceModel(entity, office);
        factory.getOfficeDao().update(office);
        return entity;
    }

    @Override
    public OfficeType get(Integer id) throws OfficeNotFoundException {
        Office office = factory.getOfficeDao().findById(id);
        if (null == office) {
            throw new OfficeNotFoundException(String.valueOf(id));
        }
        OfficeType officeType = new OfficeType();
        convertToPresentationModel(office, officeType);
        return officeType;
    }

    @Override
    public List<OfficeType> getAll() {
        List<Office> offices = factory.getOfficeDao().findAll();
        List<OfficeType> officeTypes = new ArrayList<>(offices.size());
        if (!offices.isEmpty()) {
            for (Office office : offices) {
                OfficeType officeType = new OfficeType();
                convertToPresentationModel(office, officeType);
                officeTypes.add(officeType);
            }
        }
        return officeTypes;
    }

    @Override
    public OfficeType delete(Integer id) {
        Office office = factory.getOfficeDao().delete(id);
        OfficeType officeType = new OfficeType();
        if (null != office) {
            convertToPresentationModel(office, officeType);
        }
        return officeType;
    }
}
