package com.epam.mentorship.persistence;

public class WorkPlaceDomain {

    protected int number;
    protected Coordinate coordinate;
    protected Room room;
    protected Employee employee;

    public WorkPlaceDomain(int number, Coordinate coordinate, Room room, Employee employee) {
        this.number = number;
        this.coordinate = coordinate;
        this.room = room;
        this.employee = employee;
    }

    public WorkPlaceDomain() {
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public static class Coordinate {

        protected Integer row;
        protected Integer column;

        public Coordinate(Integer row, Integer column) {
            this.row = row;
            this.column = column;
        }

        public Integer getRow() {
            return row;
        }

        public void setRow(Integer value) {
            this.row = value;
        }

        public Integer getColumn() {
            return column;
        }

        public void setColumn(Integer value) {
            this.column = value;
        }

    }

}
