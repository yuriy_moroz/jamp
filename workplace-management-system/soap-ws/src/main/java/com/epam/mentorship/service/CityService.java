package com.epam.mentorship.service;

import com.epam.mentorship.persistence.City;
import com.epam.mentorship.presentation.domain.CityType;
import com.epam.mentorship.presentation.exception.CityNotFoundException;

public interface CityService
        extends CrudService<Integer, CityType, CityNotFoundException>,
        Converter<CityType, City> {
}
