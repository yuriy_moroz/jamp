package com.epam.mentorship.service;

import com.epam.mentorship.persistence.Address;
import com.epam.mentorship.presentation.domain.AddressType;
import com.epam.mentorship.presentation.exception.AddressNotFoundException;

public interface AddressService
        extends CrudService<Integer, AddressType, AddressNotFoundException>,
        Converter<AddressType, Address> {
}
