package com.epam.mentorship.presentation.exception;

public class OfficeNotFoundException extends Exception {
    public OfficeNotFoundException(String id) {
        super("Office isn't found for id "+id);
    }
}
