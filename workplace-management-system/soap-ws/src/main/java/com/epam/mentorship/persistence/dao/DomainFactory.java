package com.epam.mentorship.persistence.dao;

public interface DomainFactory {

    CountryDao getCountryDao();

    CityDao getCityDao();

    AddressDao getAddressDao();

    EmployeeDao getEmployeeDao();

    CompanyDao getCompanyDao();

    OfficeDao getOfficeDao();

    AreaDao getAreaDao();

    RoomDao getRoomDao();

    WorkPlaceDao getWorkPlaceDao();
}
