package com.epam.mentorship.service;

import com.epam.mentorship.persistence.Employee;
import com.epam.mentorship.presentation.domain.EmployeeType;
import com.epam.mentorship.presentation.exception.EmployeeNotFoundException;

public interface EmployeeService
        extends CrudService<Integer, EmployeeType, EmployeeNotFoundException>,
        Converter<EmployeeType, Employee> {
}
