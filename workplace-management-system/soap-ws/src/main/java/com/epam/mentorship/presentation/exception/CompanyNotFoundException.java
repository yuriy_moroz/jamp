package com.epam.mentorship.presentation.exception;

public class CompanyNotFoundException extends Exception {
    public CompanyNotFoundException(String id) {
        super("Company isn't found for id "+id);
    }
}
