package com.epam.mentorship.presentation;

import com.epam.mentorship.presentation.exception.AddressNotFoundException;
import com.epam.mentorship.presentation.exception.AreaNotFoundException;
import com.epam.mentorship.presentation.exception.CityNotFoundException;
import com.epam.mentorship.presentation.exception.CompanyNotFoundException;
import com.epam.mentorship.presentation.exception.CountryNotFoundException;
import com.epam.mentorship.presentation.exception.EmployeeNotFoundException;
import com.epam.mentorship.presentation.exception.OfficeNotFoundException;
import com.epam.mentorship.presentation.exception.RoomNotFoundException;
import com.epam.mentorship.presentation.exception.WorkPlaceNotFoundException;
import com.epam.mentorship.presentation.domain.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.List;

@WebService
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT)
public interface WorkplaceManagementSystemService {

    @WebMethod(action = "addCountry")
    @WebResult(name="country")
    CountryType addCountry(@WebParam(name = "country") CountryType countryType);

    @WebMethod(action = "updateCountry")
    @WebResult(name="country")
    CountryType updateCountry(@WebParam(name = "country") CountryType countryType) throws CountryNotFoundException;

    @WebMethod(action = "getCountry")
    @WebResult(name="country")
    CountryType getCountry(@WebParam(name = "countryName") String name) throws CountryNotFoundException;

    @WebMethod(action = "getAllCountries")
    @WebResult(name="country")
    List<CountryType> getAllCountries();

    @WebMethod(action = "deleteCountry")
    @WebResult(name="country")
    CountryType deleteCountry(@WebParam(name = "countryName") String name);

    @WebMethod(action = "addCity")
    @WebResult(name="city")
    CityType addCity(@WebParam(name = "city") CityType cityType);

    @WebMethod(action = "updateCity")
    @WebResult(name="city")
    CityType updateCity(@WebParam(name = "city") CityType cityType) throws CityNotFoundException;

    @WebMethod(action = "getCity")
    @WebResult(name="city")
    CityType getCity(@WebParam(name = "cityId") Integer id) throws CityNotFoundException;

    @WebMethod(action = "getAllCities")
    @WebResult(name="city")
    List<CityType> getAllCities();

    @WebMethod(action = "deleteCity")
    @WebResult(name="city")
    CityType deleteCity(@WebParam(name = "cityId") Integer id);

    @WebMethod(action = "addAddressType")
    @WebResult(name="address")
    AddressType addAddressType(@WebParam(name = "address") AddressType addressType);

    @WebMethod(action = "updateAddress")
    @WebResult(name="address")
    AddressType updateAddress(@WebParam(name = "address") AddressType addressType) throws AddressNotFoundException;

    @WebMethod(action = "getAddress")
    @WebResult(name="address")
    AddressType getAddress(@WebParam(name = "addressId") Integer id) throws AddressNotFoundException;

    @WebMethod(action = "getAllAddresses")
    @WebResult(name="address")
    List<AddressType> getAllAddresses();

    @WebMethod(action = "deleteAddress")
    @WebResult(name="address")
    AddressType deleteAddress(@WebParam(name = "addressId") Integer id);

    @WebMethod(action = "addEmployee")
    @WebResult(name="employee")
    EmployeeType addEmployee(@WebParam(name = "employee") EmployeeType employeeType);

    @WebMethod(action = "getEmployee")
    @WebResult(name="employee")
    EmployeeType getEmployee(@WebParam(name = "employeeId") Integer id) throws EmployeeNotFoundException;

    @WebMethod(action = "getAllEmployees")
    @WebResult(name="employee")
    List<EmployeeType> getAllEmployees();

    @WebMethod(action = "updateEmployee")
    @WebResult(name="employee")
    EmployeeType updateEmployee(@WebParam(name = "employee") EmployeeType employeeType) throws EmployeeNotFoundException;

    @WebMethod(action = "deleteEmployee")
    @WebResult(name="employee")
    EmployeeType deleteEmployee(@WebParam(name = "employeeId") Integer id);

    @WebMethod(action = "addCompany")
    @WebResult(name="company")
    CompanyType addCompany(@WebParam(name = "company") CompanyType companyType);

    @WebMethod(action = "updateCompany")
    @WebResult(name="company")
    CompanyType updateCompany(@WebParam(name = "company") CompanyType companyType) throws CompanyNotFoundException;

    @WebMethod(action = "getCompany")
    @WebResult(name="company")
    CompanyType getCompany(@WebParam(name = "companyId") Integer id) throws CompanyNotFoundException;

    @WebMethod(action = "getAllCompanies")
    @WebResult(name="company")
    List<CompanyType> getAllCompanies();

    @WebMethod(action = "deleteCompany")
    @WebResult(name="company")
    CompanyType deleteCompany(@WebParam(name = "companyId") Integer id);

    @WebMethod(action = "addOffice")
    @WebResult(name="office")
    OfficeType addOffice(@WebParam(name = "office") OfficeType officeType);

    @WebMethod(action = "updateOffice")
    @WebResult(name="office")
    OfficeType updateOffice(@WebParam(name = "office") OfficeType officeType) throws OfficeNotFoundException;

    @WebMethod(action = "getOffice")
    @WebResult(name="office")
    OfficeType getOffice(@WebParam(name = "officeId") Integer id) throws OfficeNotFoundException;

    @WebMethod(action = "getAllOffices")
    @WebResult(name="office")
    List<OfficeType> getAllOffices();

    @WebMethod(action = "deleteOffice")
    @WebResult(name="office")
    OfficeType deleteOffice(@WebParam(name = "officeId") Integer id);

    @WebMethod(action = "addArea")
    @WebResult(name="area")
    AreaType addArea(@WebParam(name = "area") AreaType areaType);

    @WebMethod(action = "updateArea")
    @WebResult(name="area")
    AreaType updateArea(@WebParam(name = "area") AreaType areaType) throws AreaNotFoundException;

    @WebMethod(action = "getArea")
    @WebResult(name="area")
    AreaType getArea(@WebParam(name = "areaId") Integer id) throws AreaNotFoundException;

    @WebMethod(action = "getAllAreas")
    @WebResult(name="area")
    List<AreaType> getAllAreas();

    @WebMethod(action = "deleteArea")
    @WebResult(name="area")
    AreaType deleteArea(@WebParam(name = "areaId") Integer id);

    @WebMethod(action = "addRoom")
    @WebResult(name="room")
    RoomType addRoom(@WebParam(name = "room") RoomType roomType);

    @WebMethod(action = "updateRoom")
    @WebResult(name="room")
    RoomType updateRoom(@WebParam(name = "room") RoomType roomType) throws RoomNotFoundException;

    @WebMethod(action = "getRoom")
    @WebResult(name="room")
    RoomType getRoom(@WebParam(name = "roomId") Integer id) throws RoomNotFoundException;

    @WebMethod(action = "getAllRooms")
    @WebResult(name="room")
    List<RoomType> getAllRooms();

    @WebMethod(action = "deleteRoom")
    @WebResult(name="room")
    RoomType deleteRoom(@WebParam(name = "roomId") Integer id);



    @WebMethod(action = "addWorkPlace")
    @WebResult(name="workPlace")
    WorkPlace addWorkPlace(@WebParam(name = "workPlace") WorkPlace workPlace);

    @WebMethod(action = "updateWorkPlace")
    @WebResult(name="workPlace")
    WorkPlace updateWorkPlace(@WebParam(name = "workPlace") WorkPlace workPlace) throws WorkPlaceNotFoundException;

    @WebMethod(action = "getWorkPlace")
    @WebResult(name="workPlace")
    WorkPlace getWorkPlace(@WebParam(name = "workPlaceNumber") Integer number) throws WorkPlaceNotFoundException;

    @WebMethod(action = "getAllWorkPlaces")
    @WebResult(name="workPlace")
    List<WorkPlace> getAllWorkPlaces();

    @WebMethod(action = "deleteWorkPlace")
    @WebResult(name="workPlace")
    WorkPlace deleteWorkPlace(@WebParam(name = "workPlaceNumber") Integer number);
}
