package com.epam.mentorship.persistence.dao.impl;

import com.epam.mentorship.persistence.Address;
import com.epam.mentorship.persistence.Area;
import com.epam.mentorship.persistence.City;
import com.epam.mentorship.persistence.Color;
import com.epam.mentorship.persistence.Company;
import com.epam.mentorship.persistence.Country;
import com.epam.mentorship.persistence.Employee;
import com.epam.mentorship.persistence.Locale;
import com.epam.mentorship.persistence.Office;
import com.epam.mentorship.persistence.PrimarySkill;
import com.epam.mentorship.persistence.Room;
import com.epam.mentorship.persistence.WorkPlaceDomain;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class DomainStorage {

    private static DomainStorage instance;

    private AtomicInteger addressCounter = new AtomicInteger();
    private AtomicInteger employeeCounter = new AtomicInteger();
    private AtomicInteger cityCounter = new AtomicInteger();
    private AtomicInteger companyCounter = new AtomicInteger();
    private AtomicInteger officeCounter = new AtomicInteger();
    private AtomicInteger areaCounter = new AtomicInteger();
    private AtomicInteger roomCounter = new AtomicInteger();
    private AtomicInteger workPlaceCounter = new AtomicInteger();

    private DomainStorage() {}

    public static DomainStorage getInstance() {
        if (null == instance) {
            instance = new DomainStorage();
        }
        return instance;
    }

    private Map<String, Country> countryMap = new ConcurrentHashMap<String, Country>(){{
        put("Ukraine",
                new Country("Ukraine", Collections.singletonList(Locale.UK_UA), "ISO 3166-2:UA"));
    }};
    private Map<Integer, City> cityMap = new ConcurrentHashMap<Integer, City>(){{
        Integer id = cityCounter.incrementAndGet();
        put(id, new City(id, "Lviv", countryMap.get("Ukraine")));
    }};
    private Map<Integer, Address> addressMap = new ConcurrentHashMap<Integer, Address>(){{
        Integer id = addressCounter.incrementAndGet();
        put(id, new Address(id, cityMap.get(1), "Street", "44b", "4555", 2));
    }};
    private Map<Integer, Company> companyMap = new ConcurrentHashMap<Integer, Company>(){{
        Integer id = companyCounter.incrementAndGet();
        put(id, new Company(id, "Epam", null));
    }};
    private Map<Integer, Office> officeMap = new ConcurrentHashMap<Integer, Office>(){{
        Integer id = officeCounter.incrementAndGet();
        put(id, new Office(id, "Lviv Office", companyMap.get(1), addressMap.get(1),
                Office.Type.OPEN_SPACE, 1000, 5, 3000, 5));
    }};
    private Map<Integer, Area> areaMap = new ConcurrentHashMap<Integer, Area>(){{
        Integer id = areaCounter.incrementAndGet();
        put(id, new Area(id, "A", officeMap.get(1), 400, 3, 600, 20, 2, Color.RED));
    }};

    private Map<Integer, Room> roomMap = new ConcurrentHashMap<Integer, Room>(){{
        Integer id = roomCounter.incrementAndGet();
        put(id, new Room(id, 7, areaMap.get("A"), 12, 3, 2));
    }};

    private Map<Integer, Employee> employeeMap = new ConcurrentHashMap<Integer, Employee>(){{
        Integer id = employeeCounter.incrementAndGet();
        put(id, new Employee(id, "Vasia Pupkin", PrimarySkill.JAVA, "developer"));
    }};

    private Map<Integer, WorkPlaceDomain> workPlaceMap = new ConcurrentHashMap<Integer, WorkPlaceDomain>(){{
        Integer id = workPlaceCounter.incrementAndGet();
        put(id, new WorkPlaceDomain(id, new WorkPlaceDomain.Coordinate(1, 2), roomMap.get(1), employeeMap.get(1)));
    }};

    public AtomicInteger getAddressCounter() {
        return addressCounter;
    }

    public AtomicInteger getEmployeeCounter() {
        return employeeCounter;
    }

    public AtomicInteger getCityCounter() {
        return cityCounter;
    }

    public AtomicInteger getCompanyCounter() {
        return companyCounter;
    }

    public AtomicInteger getOfficeCounter() {
        return officeCounter;
    }

    public AtomicInteger getRoomCounter() {
        return roomCounter;
    }

    public AtomicInteger getWorkPlaceCounter() {
        return workPlaceCounter;
    }

    public Map<String, Country> getCountryMap() {
        return countryMap;
    }

    public Map<Integer, City> getCityMap() {
        return cityMap;
    }

    public Map<Integer, Address> getAddressMap() {
        return addressMap;
    }

    public Map<Integer, Company> getCompanyMap() {
        return companyMap;
    }

    public Map<Integer, Employee> getEmployeeMap() {
        return employeeMap;
    }

    public Map<Integer, Office> getOfficeMap() {
        return officeMap;
    }

    public Map<Integer, Area> getAreaMap() {
        return areaMap;
    }

    public Map<Integer, Room> getRoomMap() {
        return roomMap;
    }

    public Map<Integer, WorkPlaceDomain> getWorkPlaceMap() {
        return workPlaceMap;
    }
}
