package com.epam.mentorship.persistence;

public enum Locale {

    EN_US("en_US"),
    FR_CA("fr_CA"),
    UK_UA("uk_UA");
    private final String value;

    Locale(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Locale fromValue(String v) {
        for (Locale c: Locale.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
