package com.epam.mentorship.persistence.dao;

import java.util.List;

public interface CrudDao<I, T> {

    T create(T entity);

    T update(T entity);

    T findById(I id);

    List<T> findAll();

    T delete(I id);
}
