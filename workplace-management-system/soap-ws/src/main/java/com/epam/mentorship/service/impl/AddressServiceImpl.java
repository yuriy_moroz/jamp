package com.epam.mentorship.service.impl;

import com.epam.mentorship.persistence.dao.DomainFactory;
import com.epam.mentorship.persistence.dao.impl.DomainFactoryImpl;
import com.epam.mentorship.persistence.Address;
import com.epam.mentorship.presentation.domain.AddressType;
import com.epam.mentorship.presentation.exception.AddressNotFoundException;
import com.epam.mentorship.service.AddressService;

import java.util.ArrayList;
import java.util.List;

public class AddressServiceImpl implements AddressService {

    private DomainFactory factory;

    public AddressServiceImpl() {
        factory = new DomainFactoryImpl();
    }

    @Override
    public void convertToPersistenceModel(AddressType from, Address to) {
        to.setApartmentNumber(from.getApartmentNumber());
        to.setCity(factory.getCityDao().findById(from.getCityId()));
        to.setHouseNumber(from.getHouseNumber());
        to.setPostCode(from.getPostCode());
        to.setStreet(from.getStreet());
    }

    @Override
    public void convertToPresentationModel(Address from, AddressType to) {
        to.setId(from.getId());
        to.setApartmentNumber(from.getApartmentNumber());
        to.setPostCode(from.getPostCode());
        to.setStreet(from.getStreet());
        to.setCityId(from.getCity().getId());
        to.setHouseNumber(from.getHouseNumber());
    }

    @Override
    public AddressType add(AddressType entity) {
        Address address = new Address();
        convertToPersistenceModel(entity, address);
        factory.getAddressDao().create(address);
        entity.setId(address.getId());
        return entity;
    }

    @Override
    public AddressType update(AddressType entity) throws AddressNotFoundException {
        Address address = factory.getAddressDao().findById(entity.getId());
        if (null == address) {
            throw new AddressNotFoundException(String.valueOf(entity.getId()));
        }
        convertToPersistenceModel(entity, address);
        factory.getAddressDao().update(address);
        return entity;
    }

    @Override
    public AddressType get(Integer id) throws AddressNotFoundException {
        Address address = factory.getAddressDao().findById(id);
        if (null == address) {
            throw new AddressNotFoundException(id.toString());
        }
        AddressType addressType = new AddressType();
        convertToPresentationModel(address, addressType);
        return addressType;
    }

    @Override
    public List<AddressType> getAll() {
        List<Address> addresses = factory.getAddressDao().findAll();
        List<AddressType> addressTypes = new ArrayList<>(addresses.size());
        if (!addresses.isEmpty()) {
            for (Address address : addresses) {
                AddressType addressType = new AddressType();
                convertToPresentationModel(address, addressType);
                addressTypes.add(addressType);
            }
        }
        return addressTypes;
    }

    @Override
    public AddressType delete(Integer id) {
        Address address = factory.getAddressDao().delete(id);
        AddressType addressType = new AddressType();
        if (null == address) {
            return addressType;
        }
        convertToPresentationModel(address, addressType);
        return addressType;
    }
}
